#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#define MAXBUFF 1024  // numero de caract. do buffer
void client (int, int);
void server(int, int);

int main()
{
   int merda;
   int	descritor,  // usado para criar o processo filho pelo fork
	pipe1[2],  // comunicacao pai -> filho
	pipe2[2];  // comunicacao filho -> pai

   if (pipe(pipe1)<0 || pipe(pipe2) <0)
	{ printf("Erro na chamada PIPE");
	   exit(0);
	}

   //   Fork para criar o processo filho

    if ( (descritor = fork()) <0)
	{ printf("Erro na chamada FORK");
	   exit(0);
	}

	else if (descritor >0)  // PROCESSO PAI
	   {	close(pipe1[0]); // fecha leitura no pipe1
		close(pipe2[1]);  // fecha escrita no pipe2

		client(pipe2[0], pipe1[1]);  // Chama CLIENTE no PAI

		close(pipe1[1]); // fecha pipe1
		close(pipe2[0]);  // fecha pipe2
		exit(0);

	    } // FIM DO PROCESSO PAI

	else // PROCESSO FILHO

	   {	close(pipe1[1]); // fecha escrita no pipe1
		close(pipe2[0]);  // fecha leitura no pipe2

		server(pipe1[0], pipe2[1]);  // Chama SERVIDOR no FILHO

		close(pipe1[0]); // fecha leitura no pipe1
		close(pipe2[1]);  // fecha escrita no pipe2
		exit(0);

	} // FIM DO PROCESSO FILHO
} // FIM DO MAIN

/* -----------------------------------------------------------------------------------------------------------
Funcao Client: 	Executa no processo PAI
			Envia o nome do arquivo para o FILHO
			Recebe os dados do FILHO e imprime na tela
----------------------------------------------------------------------------------------------------------- */

void client (readfd, writefd)

int readfd, // leitura do pipe2[0]
    writefd; // escrita no pipe1[1]

{
	char buff[MAXBUFF];
	int n;
// Le o nome do arquivo da entrada padrao (teclado)
// e envia para o servidor atraves do pipe

	printf(" \n Entre com o nome do arquivo ->");

    
   if (scanf("%[^\n]", buff) == 0)
	{
	printf("Funcao client: erro na leitura do nome do arquivo");
	exit(0);
	}
// Envia o nome do arquivo p/ o FILHO pelo pipe1
	n = strlen(buff);
	if (write(writefd, buff, n) != n)
	  {printf("Funcao Client: Erro no envio do nome do arquivo");
	   exit(0);
	  }

// Le os dados vindos do servidor e escreve-os p/ saida padrao (video)
       while ( (n=read(readfd,buff,MAXBUFF) ) > 0)
	  if (write(1,buff,n) != n)
	      { // fd = 1, saida padrao
		printf("Funcao Client: Erro na escrita para o video");
		exit(0);
	      }
	      else if (n<0)
		     {printf("Fun��o Client: Erro na leitura do pipe");
		      exit(0);
		     }

	  } // Fim da Funcao CLIENT

/* -----------------------------------------------------------------------------------------------------------
Funcao Server: 		Executa no processo FILHO
			Abre o arquivo solicitado e envia seu conteudo
			para o PAI
----------------------------------------------------------------------------------------------------------- */
void server(readfd, writefd)
int readfd, // leitura do pipe1[0]
    writefd; // escrita no pipe2[1]

{
	char buff[MAXBUFF];
	int n, fd;

	// le o nome do arquivo enviado pelo cliente atraves do pipe1
	if ( ( n= read(readfd, buff, MAXBUFF) ) <=0)
	   { printf("Funcao Server: Erro no recebimento do nome do arquivo");
	     exit(0);
	   }


        buff[n]='\0';  // tirando o 'lixo' do final da vari�vel

       // Abre o arquivo para enviar o seu conteudo para client no pai
       if ( ( fd=open(buff,0)) <0)
	   { // se houve erro, envia msg de erro ao cliente
	     sprintf(buff,"Erro: Servidor n�o consegue abrir arquivo solicitado");
	     n = strlen(buff);
	     if (write(writefd, buff,n) !=n){
		printf("Funcao server: Erro no envio da msg de erro!");
		exit(0);
         }
	   }
	 else { // se conseguiu abrir o arquivo
		// le os dados e envia para client
		while((n=read(fd,buff,MAXBUFF)) >0){
		   if (write(writefd,buff,n) != n)
		      {printf("Funcao server: Erro no envio do conteudo do arquivo");
		       exit(0);
		      }
		   if (n<0)
		      {printf("Funcao Server: erro na leitura do conteudo do arquivo");
		      exit(0);
		      }
        }
	  }
    } // Fim da Funcao Server
